import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import { Home } from './Home';
import { Header } from './components/Header';
import SignIn from './Admin';
// import '/assets/base.scss';
function App() {
  return (
    <React.Fragment>
      <Router>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/login" component={SignIn} />
          {/* <Route component={NoMatch} /> */}
        </Switch>
      </Router>
    </React.Fragment>
  );
}

export default App;
